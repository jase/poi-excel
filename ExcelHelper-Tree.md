<details open="true">
    <summary>ExcelHelper <em style="color: red">导入导出引导类</em></summary>
    <ul style="list-style:none;">
        <li style="list-style:none; margin-left: -15px">
            <details>
                <summary>opsExport <em style="color: red">导出Excel</em></summary>
                <ul style="list-style:none;">
                    <li style="list-style:none; margin-left: -15px">
                        <details>
                            <summary>opsSheet <em style="color: red">声明sheet</em></summary>
                            <ul style="list-style:none;">
                                <li style="list-style:none; margin-left: -15px">
                                    <details>
                                        <summary>opsHeader <em style="color: red">设置表头</em></summary>
                                        <ul style="list-style:none;">
                                            <li style="list-style:none; margin-left: -15px">
                                                <details>
                                                    <summary>complex <em style="color: red">复杂表头</em></summary>
                                                    <ul style="list-style:none;">
                                                        <li style="list-style:none; margin-left: -15px">text <em style="color: red">单元格声明</em></li>
                                                    </ul>
                                                </details>
                                            </li>
                                            <li style="list-style:none; margin-left: -15px">
                                                <details>
                                                    <summary>simple <em style="color: red">简单表头</em></summary>
                                                    <ul style="list-style:none;">
                                                        <li style="list-style:none; margin-left: -15px">title <em style="color: red">大标题</em></li>
                                                        <li style="list-style:none; margin-left: -15px">text <em style="color: red">列标题</em></li>
                                                        <li style="list-style:none; margin-left: -15px">texts <em style="color: red">列标题批量</em></li>
                                                    </ul>
                                                </details>
                                            </li>
                                            <li style="list-style:none; margin-left: -15px">noFreeze <em style="color: red">不冻结表头</em></li>
                                        </ul>
                                    </details>
                                </li>
                                <li style="list-style:none; margin-left: -15px">
                                    <details>
                                        <summary>opsColumn <em style="color: red">设置导出字段</em></summary>
                                        <ul style="list-style:none;">
                                            <li style="list-style:none; margin-left: -15px">
                                                <details>
                                                    <summary>field <em style="color: red">字段设置</em></summary>
                                                    <ul style="list-style:none;">
                                                        <li style="list-style:none; margin-left: -15px">color <em style="color: red">字体颜色</em></li>
                                                        <li style="list-style:none; margin-left: -15px">width <em style="color: red">宽度</em></li>
                                                        <li style="list-style:none; margin-left: -15px">height <em style="color: red">高度</em></li>
                                                        <li style="list-style:none; margin-left: -15px">wrapText <em style="color: red">自动换行</em></li>
                                                        <li style="list-style:none; margin-left: -15px">align <em style="color: red">水平定位</em></li>
                                                        <li style="list-style:none; margin-left: -15px">backColor <em style="color: red">背景色</em></li>
                                                        <li style="list-style:none; margin-left: -15px">pattern <em style="color: red">内容格式化</em></li>
                                                        <li style="list-style:none; margin-left: -15px">dropdown <em style="color: red">下拉框</em></li>
                                                        <li style="list-style:none; margin-left: -15px">comment <em style="color: red">注释</em></li>
                                                        <li style="list-style:none; margin-left: -15px">mergerRepeat <em style="color: red">纵向自动合并</em></li>
                                                        <li style="list-style:none; margin-left: -15px">valign <em style="color: red">垂直定位</em></li>
                                                        <li style="list-style:none; margin-left: -15px">verifyIntNum <em style="color: red">验证整数</em></li>
                                                        <li style="list-style:none; margin-left: -15px">verifyFloatNum <em style="color: red">验证浮点数字</em></li>
                                                        <li style="list-style:none; margin-left: -15px">verifyDate <em style="color: red">验证日期</em></li>
                                                        <li style="list-style:none; margin-left: -15px">verifyText <em style="color: red">验证单元格</em></li>
                                                        <li style="list-style:none; margin-left: -15px">verifyCustom <em style="color: red">自定义验证</em></li>
                                                        <li style="list-style:none; margin-left: -15px">outHandle <em style="color: red">输出回调钩子</em></li>                                                   
                                                     </ul>
                                                </details>
                                            </li>
                                            <li style="list-style:none; margin-left: -15px">fields <em style="color: red">批量字段设置</em></li>
                                        </ul>
                                    </details>
                                </li>
                                <li style="list-style:none; margin-left: -15px">
                                    <details>
                                        <summary>opsFooter <em style="color: red">设置表尾</em></summary>
                                        <ul style="list-style:none;">
                                            <li style="list-style:none; margin-left: -15px">text <em style="color: red">单元格内容</em></li>
                                        </ul>
                                    </details>
                                </li>
                                <li style="list-style:none; margin-left: -15px">sheetName <em style="color: red">sheet名称</em></li>
                                <li style="list-style:none; margin-left: -15px">width <em style="color: red">统一宽度</em></li>
                                <li style="list-style:none; margin-left: -15px">height <em style="color: red">统一高度</em></li>
                                <li style="list-style:none; margin-left: -15px">autoNum <em style="color: red">自动序号</em></li>
                                <li style="list-style:none; margin-left: -15px">autoNumColumnWidth <em style="color: red">自动序号列宽度</em></li>
                                <li style="list-style:none; margin-left: -15px">mergeCells <em style="color: red">批量合并单元格</em></li>
                                <li style="list-style:none; margin-left: -15px">mergeCellsIndex <em style="color: red">批量合并单元格(下标形式)</em></li>
                                <li style="list-style:none; margin-left: -15px">mergeCell <em style="color: red">合并单元格</em></li>
                            </ul>
                        </details>
                    </li>
                    <li style="list-style:none; margin-left: -15px">createBook <em style="color: red">memo</em></li>
                    <li style="list-style:none; margin-left: -15px">fillBook <em style="color: red">memo</em></li>
                    <li style="list-style:none; margin-left: -15px">parallelSheet <em style="color: red">memo</em></li>
                    <li style="list-style:none; margin-left: -15px">style <em style="color: red">memo</em></li>
                    <li style="list-style:none; margin-left: -15px">export <em style="color: red">memo</em></li>
                    <li style="list-style:none; margin-left: -15px">password <em style="color: red">memo</em></li>
                </ul>
            </details>
        </li>
        <li style="list-style:none; margin-left: -15px">
            <details>
                <summary>opsReplace <em style="color: red">读模板导出Excel</em></summary>
                <ul style="list-style:none;">
                    <li style="list-style:none; margin-left: -15px">from <em style="color: red">文件源</em></li>
                    <li style="list-style:none; margin-left: -15px">variable <em style="color: red">变量替换</em></li>                    
                    <li style="list-style:none; margin-left: -15px">variables <em style="color: red">批量变量替换</em></li>
                    <li style="list-style:none; margin-left: -15px">password <em style="color: red">设置密码</em></li>
                    <li style="list-style:none; margin-left: -15px">replace <em style="color: red">输出workbook</em></li>
                    <li style="list-style:none; margin-left: -15px">replaceTo <em style="color: red">输出文件</em></li>
                </ul>
            </details>
        </li>
        <li style="list-style:none; margin-left: -15px">
            <details>
                <summary>opsParse <em style="color: red">解析Excel声明</em></summary>
                <ul style="list-style:none;">
                    <li style="list-style:none; margin-left: -15px">from <em style="color: red">文件源</em></li>
                    <li style="list-style:none; margin-left: -15px">
                        <details>
                            <summary>opsSheet <em style="color: red">解析sheet区域声明</em></summary>
                            <ul style="list-style:none;">
                                <li style="list-style:none; margin-left: -15px">
                                    <details>
                                        <summary>opsColumn <em style="color: red">解析列定义</em></summary>
                                        <ul style="list-style:none;">
                                            <li style="list-style:none; margin-left: -15px">
                                                <details>
                                                    <summary>field <em style="color: red">字段</em></summary>
                                                    <ul style="list-style:none;">
                                                        <li style="list-style:none; margin-left: -15px">notNull <em style="color: red">不能为空</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asInt <em style="color: red">类型int</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asBoolean <em style="color: red">类型boolean</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asString <em style="color: red">类型string</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asLong <em style="color: red">类型Long</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asBigDecimal <em style="color: red">类型Bigdecimal</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asDate <em style="color: red">类型Date</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asDouble <em style="color: red">类型Double</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asFloat <em style="color: red">类型Float</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asImg <em style="color: red">类型Img</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asShort <em style="color: red">类型Short</em></li>
                                                        <li style="list-style:none; margin-left: -15px">asChar <em style="color: red">类型Char</em></li> <li style="list-style:none; margin-left: -15px">asByCustom <em style="color: red">自定义类型</em></li>
                                                    </ul>
                                                </details>
                                            </li>
                                        </ul>
                                    </details>
                                </li>
                                <li style="list-style:none; margin-left: -15px">callBack <em style="color: red">解析回调钩子</em></li>
                                <li style="list-style:none; margin-left: -15px">parse <em style="color: red">解析文件</em></li>
                            </ul>
                        </details>
                    </li>
                </ul>
            </details>
        </li>
    </ul>
</details>